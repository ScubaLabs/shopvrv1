﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRStandardAssets.Utils;
using UnityEngine.SceneManagement;

public class ShopInteraction : MonoBehaviour {
    [SerializeField]
    private string shopName;
    [SerializeField]
    private VRInteractiveItem m_interactiveItem;

    private SelectionRadial m_selectionRadial;
    private bool isActive = false;
    
    // Use this for initialization
	void Start () {
       	
	}

    private void OnEnable()
    {
        m_selectionRadial = FindObjectOfType<SelectionRadial>();

        m_interactiveItem.OnOver += OnFocusEnter;
        m_interactiveItem.OnOut += OnFocusExit;
        m_selectionRadial.OnSelectionComplete += OnSelectShop;
    }

    // Update is called once per frame
    void Update () {
		
	}

    private void OnDisable()
    {
        m_interactiveItem.OnOver -= OnFocusEnter;
        m_interactiveItem.OnOut -= OnFocusExit;
        m_selectionRadial.OnSelectionComplete -= OnSelectShop;
    }

    private void OnFocusEnter()
    {
        isActive = true;
        m_selectionRadial.Show();
    }

    private void OnFocusExit()
    {
        isActive = false;
        m_selectionRadial.Hide();
    }

    private void OnSelectShop()
    {
        if(isActive)
        SceneManager.LoadScene(shopName);
    }
}
