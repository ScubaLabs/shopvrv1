﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationData : MonoBehaviour {

    [SerializeField] private GameObject[] items;
    private GameObject selectedItem;

    private void Awake()
    {

        var otherObjects = FindObjectsOfType<ApplicationData>();
        foreach (ApplicationData otherObject in otherObjects)
        {
            if (otherObject != this)
                Destroy(otherObject.gameObject, 0f);
        }

       DontDestroyOnLoad(gameObject);
        selectedItem = items[0];
    }

    public void SetSelectedItem(int i)
    {
        selectedItem = items[i];
    }

    public GameObject GetSelectedItem()
    {
        return selectedItem;
    }
}
