﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRStandardAssets.Utils;
using System;

[Serializable]
public class Item {
    public string name;
    public string price;
    public string category;
    public string color;
}

public class ItemInteractionScript : MonoBehaviour {

    #region privateFields
    [SerializeField] private Item m_item;
    [SerializeField] private SelectionRadial m_SelectionRadial;
    [SerializeField] private VRInteractiveItem m_InteractiveItem;
    [SerializeField] private UIFader m_uiFader;
    [SerializeField] private UIFader m_itemMenu;
    [SerializeField] private UIFader m_itemAddedToCartFader;
    [SerializeField] private float cartDialogueDisplayTime = 2f;
    [SerializeField] private float rotationSpeed = 100f;

    private static List<Item> items = new List<Item>();
    private float timeSinceDialogueActive = 0;
    private bool isInfoVisible = false; 
    #endregion

    #region unityBuiltIn
    // Use this for initialization
    void Start () {
        m_SelectionRadial = FindObjectOfType<SelectionRadial>();
        m_itemAddedToCartFader = GameObject.FindWithTag("ItemCartDialogue").GetComponent<UIFader>();

        m_itemAddedToCartFader.SetInvisible();
    }
	
	// Update is called once per frame
	void Update () {
        if (timeSinceDialogueActive < cartDialogueDisplayTime)
        {
            timeSinceDialogueActive += Time.deltaTime;
        }

        else
        {
            m_itemAddedToCartFader.SetInvisible();
        }

        if (isInfoVisible)
        {
            this.transform.Rotate(0, Time.deltaTime * rotationSpeed, 0);
        }

        else
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.identity, Time.deltaTime * rotationSpeed / 50f);
        }
	}

    private void OnEnable()
    {
        m_InteractiveItem.OnOver += OnHoverEnter;
        m_InteractiveItem.OnOut += OnHoverExit;
        m_InteractiveItem.OnDown += OnItemSelected;
        m_InteractiveItem.OnClick += OnTapItem;
        m_InteractiveItem.OnDoubleClick += OnAddItemToCart;

        isInfoVisible = false;
        timeSinceDialogueActive = 0;
        m_uiFader.SetInvisible();
    }

    private void OnDisable()
    {
        m_InteractiveItem.OnOver -= OnHoverEnter;
        m_InteractiveItem.OnOut -= OnHoverExit;
        m_InteractiveItem.OnDown -= OnItemSelected;
        m_InteractiveItem.OnClick -= OnTapItem;
        m_InteractiveItem.OnDoubleClick -= OnAddItemToCart;
    }
    #endregion

    #region ItemInteraction
    private void OnHoverEnter()
    {
        m_SelectionRadial.Show();
        if (m_itemMenu)
        {
            m_itemMenu.SetVisible();
        }
    }

    private void OnHoverExit()
    {
        m_SelectionRadial.Hide();
        if (m_itemMenu)
        {
            m_itemMenu.SetInvisible();
        }
    }

    private void OnItemSelected()
    {

    }

    private void OnTapItem()
    {

        if (isInfoVisible)
        {
            m_uiFader.SetInvisible();
            isInfoVisible = false;
        }

        else
        {
            m_uiFader.SetVisible();
            isInfoVisible = true;
            
        }
    }

    private void OnAddItemToCart()
    {
        if (!items.Contains(m_item))
        {
            items.Add(m_item);
            timeSinceDialogueActive = 0;
            m_itemAddedToCartFader.SetVisible();
        }
    }
    #endregion
}
