﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRStandardAssets.Utils;

public class ItemMenu : MonoBehaviour {

    [SerializeField] private VRInteractiveItem m_interactiveItem;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnEnable()
    {
        m_interactiveItem.OnOver += OnEnterHover;
        m_interactiveItem.OnOut += OnExitHover;
        m_interactiveItem.OnClick += OnClick;
    }

    private void OnDisable()
    {
        m_interactiveItem.OnOver -= OnEnterHover;
        m_interactiveItem.OnOut -= OnExitHover;
        m_interactiveItem.OnClick -= OnClick;
    }

    private void OnEnterHover()
    {

    }

    private void OnExitHover()
    {

    }

    private void OnClick()
    {

    }
}
