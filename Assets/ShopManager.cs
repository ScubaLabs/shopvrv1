﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class ShopManager : MonoBehaviour
{

    private void Awake()
    {

        var selectedItem = FindObjectOfType<ApplicationData>().GetSelectedItem();
        Instantiate(selectedItem, selectedItem.transform.position, selectedItem.transform.rotation);
    }

}
